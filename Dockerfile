FROM golang:1.19-alpine AS build
WORKDIR /app
COPY . ./
RUN go mod download
RUN CGO_ENABLED=0 go build -installsuffix 'static' -o /backfeed .
FROM gcr.io/distroless/static
WORKDIR /
COPY --from=build /backfeed /backfeed
ENTRYPOINT ["/backfeed"]
